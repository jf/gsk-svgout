use glib::translate::*;
use gsk4::prelude::*;
use gtk4::{glib, graphene};
use heck::ToKebabCase;
use maud::{html, Markup};
use std::{
    collections::HashMap,
    io::{Read, Write},
    path::PathBuf,
};

extern "C" {
    pub fn pango_fc_font_lock_face(font: *mut std::ffi::c_void) -> freetype_sys::FT_Face;
    pub fn pango_fc_font_unlock_face(font: *mut std::ffi::c_void);
}

#[inline]
fn color_str(c: &gsk4::gdk::RGBA) -> String {
    format!(
        "#{:02x}{:02x}{:02x}",
        (c.red() * 255.0) as u8,
        (c.green() * 255.0) as u8,
        (c.blue() * 255.0) as u8,
    )
}

#[inline]
fn id_url(id: &str) -> String {
    format!("url(#{})", id)
}

#[inline]
fn size_to_tuple(s: &graphene::Size) -> (f32, f32) {
    (s.width(), s.height())
}

fn rounded_rect_path(
    (x, y, w, h): (f32, f32, f32, f32),
    (tl_w, tl_h): (f32, f32),
    (tr_w, tr_h): (f32, f32),
    (br_w, br_h): (f32, f32),
    (bl_w, bl_h): (f32, f32),
) -> String {
    const ARC: f32 = (std::f32::consts::SQRT_2 - 1.0) * 4.0 / 3.0;
    format!(
        "M {} {} l {} {} c {} {} {} {} {} {} l {} {} c {} {} {} {} {} {} \
                 l {} {} c {} {} {} {} {} {} l {} {} c {} {} {} {} {} {} z ",
        x + tl_w,
        y,
        w - tl_w - tr_w,
        0,
        tr_w * ARC,
        0,
        tr_w,
        tr_h * (1.0 - ARC),
        tr_w,
        tr_h,
        0,
        h - tr_h - br_h,
        0,
        br_h * ARC,
        -br_w * (1.0 - ARC),
        br_h,
        -br_w,
        br_h,
        -(w - bl_w - br_w),
        0,
        -bl_w * ARC,
        0,
        -bl_w,
        -bl_h * (1.0 - ARC),
        -bl_w,
        -bl_h,
        0,
        -(h - tl_h - bl_h),
        0,
        -tl_h * ARC,
        tl_w * (1.0 - ARC),
        -tl_h,
        tl_w,
        -tl_h,
    )
}

fn gsk_rounded_rect_path(rect: &gsk4::RoundedRect) -> String {
    let bounds = rect.bounds();
    let x = bounds.x();
    let y = bounds.y();
    let w = bounds.width();
    let h = bounds.height();
    let corner = rect.corner();
    rounded_rect_path(
        (x, y, w, h),
        size_to_tuple(&corner[0]),
        size_to_tuple(&corner[1]),
        size_to_tuple(&corner[2]),
        size_to_tuple(&corner[3]),
    )
}

#[inline]
fn rect_path(x: f32, y: f32, w: f32, h: f32) -> String {
    format!("M {x} {y} l {w} 0 l 0 {h} l -{w} 0 z")
}

#[inline]
fn graphene_rect_path(rect: &graphene::Rect) -> String {
    rect_path(rect.x(), rect.y(), rect.width(), rect.height())
}

fn render_cairo_svg(node: &gsk4::RenderNode) -> cairo::Result<Markup> {
    let bounds = node.bounds();
    let surface = cairo::SvgSurface::for_stream(
        bounds.width() as f64,
        bounds.height() as f64,
        Vec::<u8>::new(),
    )?;
    let ctx = cairo::Context::new(&surface)?;
    node.draw(&ctx);
    let stream = surface.finish_output_stream().unwrap();
    let stream = stream.downcast::<Vec<u8>>().unwrap();
    Ok(maud::PreEscaped(
        String::from_utf8_lossy(stream.as_ref()).into_owned(),
    ))
}

fn render_cairo_png(node: &gsk4::RenderNode) -> cairo::Result<Markup> {
    let b = node.bounds();
    let surf =
        cairo::ImageSurface::create(cairo::Format::ARgb32, b.width() as i32, b.height() as i32)?;
    let ctx = cairo::Context::new(&surf)?;
    node.draw(&ctx);
    surf.flush();
    let mut bytes = Vec::<u8>::new();
    surf.write_to_png(&mut bytes).unwrap();
    let data = glib::base64_encode(&bytes);
    Ok(html! {
        image xlink:href={ "data:image/png;base64," (data) }
            x=(b.x()) y=(b.y()) width=(b.width()) height=(b.height()) {}
    })
}

#[derive(Default)]
struct TransformState {
    id_counter: u64,
    defs: Vec<Markup>,
    renderer: Option<gsk4::GLRenderer>,
    fonts: HashMap<pango::Font, HashMap<u32, char>>,
}

impl TransformState {
    #[inline]
    pub fn unique_id(&mut self) -> String {
        let counter = self.id_counter;
        self.id_counter += 1;
        format!("e{}", counter)
    }
    #[inline]
    pub fn add_def(&mut self, markup: Markup) {
        self.defs.push(markup);
    }
    pub fn render_linear_gradient(
        &mut self,
        bounds: &graphene::Rect,
        start: &graphene::Point,
        end: &graphene::Point,
        stops: &[gsk4::ColorStop],
        repeat: bool,
    ) -> Markup {
        let id = self.unique_id();
        let x = bounds.x();
        let y = bounds.y();
        let w = bounds.width();
        let h = bounds.height();
        let x1 = (start.x() - x) / w;
        let x2 = (end.x() - x) / w;
        let y1 = (start.y() - y) / h;
        let y2 = (end.y() - y) / h;
        self.add_def(html! {
            linearGradient gradientUnits="userSpaceOnUse" id=(id) x1=(x1) y1=(y1) x2=(x2) y2=(y2)
                spreadMethod=(if repeat { "repeat" } else { "pad" })
            {
                @for stop in stops {
                    stop offset=(stop.offset()) stop-color=(color_str(stop.color()))
                        stop-opacity=(stop.color().alpha())
                    {}
                }
            }
        });
        html! {
            rect fill=(id_url(&id)) x=(x) y=(y) width=(w) height=(h) {}
        }
    }
    pub fn render_radial_gradient(
        &mut self,
        bounds: &graphene::Rect,
        center: &graphene::Point,
        (hrad, vrad): (f32, f32),
        (start, end): (f32, f32),
        stops: &[gsk4::ColorStop],
        repeat: bool,
    ) -> Markup {
        let x = bounds.x();
        let y = bounds.y();
        let w = bounds.width();
        let h = bounds.height();
        let cx = center.x();
        let cy = center.y();
        let sx = hrad / (w / 2.0);
        let sy = vrad / (h / 2.0);
        let id = self.unique_id();
        self.add_def(html! {
            radialGradient gradientUnits="userSpaceOnUse" id=(id) cx=(0) cy=(0)
                gradientTransform={"translate(" (cx) ", " (cy) ") scale(" (sx) ", " (sy) ")"}
                spreadMethod=(if repeat { "repeat" } else { "pad" })
            {
                @for stop in stops {
                    stop offset=((stop.offset() * (end - start) + start))
                        stop-color=(color_str(stop.color()))
                        stop-opacity=(stop.color().alpha())
                    {}
                }
            }
        });
        html! {
            rect fill=(id_url(&id)) x=(x) y=(y) width=(w) height=(h) {}
        }
    }
}

fn transform(node: &gsk4::RenderNode, state: &mut TransformState) -> Markup {
    if let Some(node) = node.downcast_ref::<gsk4::ContainerNode>() {
        let children = (0..node.n_children()).map(|c| transform(&node.child(c), state));
        html! { @for child in children { (child) } }
    } else if let Some(node) = node.downcast_ref::<gsk4::CairoNode>() {
        match render_cairo_png(node) {
            Ok(markup) => markup,
            Err(err) => {
                eprintln!("Failed to render cairo node: {}", err);
                html! {}
            }
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::ColorNode>() {
        let b = node.bounds();
        html! {
            rect fill=(color_str(&node.color())) fill-opacity=(node.color().alpha())
                x=(b.x()) y=(b.y()) width=(b.width()) height=(b.height()) {}
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::LinearGradientNode>() {
        state.render_linear_gradient(
            &node.bounds(),
            &node.start(),
            &node.end(),
            &node.color_stops(),
            false,
        )
    } else if let Some(node) = node.downcast_ref::<gsk4::RepeatingLinearGradientNode>() {
        let node: &gsk4::LinearGradientNode = unsafe { std::mem::transmute(node) };
        state.render_linear_gradient(
            &node.bounds(),
            &node.start(),
            &node.end(),
            &node.color_stops(),
            true,
        )
    } else if let Some(node) = node.downcast_ref::<gsk4::RadialGradientNode>() {
        state.render_radial_gradient(
            &node.bounds(),
            &node.center(),
            (node.hradius(), node.vradius()),
            (node.start(), node.end()),
            &node.color_stops(),
            false,
        )
    } else if let Some(node) = node.downcast_ref::<gsk4::RepeatingRadialGradientNode>() {
        let node: &gsk4::RadialGradientNode = unsafe { std::mem::transmute(node) };
        state.render_radial_gradient(
            &node.bounds(),
            &node.center(),
            (node.hradius(), node.vradius()),
            (node.start(), node.end()),
            &node.color_stops(),
            true,
        )
    } else if let Some(node) = node.downcast_ref::<gsk4::ConicGradientNode>() {
        match render_cairo_png(node) {
            Ok(markup) => markup,
            Err(err) => {
                eprintln!("Failed to render cairo node: {}", err);
                html! {}
            }
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::BorderNode>() {
        let [t, r, b, l] = node.widths().to_owned();
        let outline = node.outline();
        let bounds = outline.bounds();
        let x = bounds.x();
        let y = bounds.y();
        let w = bounds.width();
        let h = bounds.height();
        let corner = outline.corner();
        let (tl_w, tl_h) = size_to_tuple(&corner[0]);
        let (tr_w, tr_h) = size_to_tuple(&corner[1]);
        let (br_w, br_h) = size_to_tuple(&corner[2]);
        let (bl_w, bl_h) = size_to_tuple(&corner[3]);
        let p1 = gsk_rounded_rect_path(&outline);
        let p2 = rounded_rect_path(
            (x + l, y + t, w - r - l, h - t - b),
            (0.0f32.max(tl_w - l), 0.0f32.max(tl_h - t)),
            (0.0f32.max(tr_w - r), 0.0f32.max(tr_h - t)),
            (0.0f32.max(br_w - r), 0.0f32.max(br_h - b)),
            (0.0f32.max(bl_w - l), 0.0f32.max(bl_h - b)),
        );
        let colors = node.colors();
        let (color, alpha) = if colors.iter().skip(1).all(|c| c == &colors[0]) {
            (color_str(&colors[0]), colors[0].alpha())
        } else {
            #[inline]
            fn trap(
                (x1, y1): (f32, f32),
                (x2, y2): (f32, f32),
                (x3, y3): (f32, f32),
                (x4, y4): (f32, f32),
            ) -> String {
                format!(
                    "M {} {} L {} {} L {} {} L {} {} z",
                    x1, y1, x2, y2, x3, y3, x4, y4
                )
            }
            let tl = (x, y);
            let tr = (x + w, y);
            let br = (x + w, y + h);
            let bl = (x, y + h);
            let mid = (w - h) / 2.0;
            let wm = w / 2.0 - f32::max(mid, 0.0);
            let hm = h / 2.0 - f32::max(-mid, 0.0);
            let itl = (x + wm, y + hm);
            let itr = (x + w - wm, y + hm);
            let ibr = (x + w - wm, y + h - hm);
            let ibl = (x + wm, y + h - hm);
            let id = state.unique_id();
            state.add_def(html! {
                pattern id=(id) width="100%" height="100%" {
                    path fill=(color_str(&colors[0])) fill-opacity=(colors[0].alpha())
                        d=(trap(tl, tr, itr, itl)) {}
                    path fill=(color_str(&colors[1])) fill-opacity=(colors[1].alpha())
                        d=(trap(tr, br, ibr, itr)) {}
                    path fill=(color_str(&colors[2])) fill-opacity=(colors[2].alpha())
                        d=(trap(br, bl, ibl, ibr)) {}
                    path fill=(color_str(&colors[3])) fill-opacity=(colors[3].alpha())
                        d=(trap(bl, tl, itl, ibl)) {}
                }
            });
            (id_url(&id), 1.0)
        };
        html! { path fill=(color) fill-opacity=(alpha) fill-rule="evenodd" d={ (p1) " " (p2) } {} }
    } else if let Some(node) = node.downcast_ref::<gsk4::TextureNode>() {
        let bytes = node.texture().save_to_png_bytes();
        let data = glib::base64_encode(&bytes);
        let b = node.bounds();
        html! {
            image xlink:href={ "data:image/png;base64," (data) }
                x=(b.x()) y=(b.y()) width=(b.width()) height=(b.height()) {}
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::InsetShadowNode>() {
        let outline = node.outline();
        let d = gsk_rounded_rect_path(&outline);
        let cid = state.unique_id();
        state.add_def(html! {
            clipPath id=(cid) clipPathUnits="userSpaceOnUse" {
                path clip-rule="evenodd" d=(d) {}
            }
        });

        let fid = state.unique_id();
        let dev = node.blur_radius() / 2.5;
        state.add_def(html! {
            filter id=(fid) {
                feGaussianBlur in="SourceGraphic" stdDeviation=(dev) {}
            }
        });
        let outer = graphene_rect_path(&node.bounds());
        let spread = node.spread();
        let or = outline.bounds();
        let oc = outline.corner().to_owned();
        let or = graphene::Rect::new(
            or.x() + node.dx() + spread,
            or.y() + node.dy() + spread,
            or.width() - spread * 2.0,
            or.height() - spread * 2.0,
        );
        let inner = gsk_rounded_rect_path(&gsk4::RoundedRect::new(or, oc[0], oc[1], oc[2], oc[3]));
        html! {
            g clip-path=(id_url(&cid)) {
                path filter=(id_url(&fid))
                    fill=(color_str(&node.color()))
                    fill-opacity=(node.color().alpha())
                    fill-rule="evenodd"
                    d={ (outer) " " (inner) }
                {}
            }
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::OutsetShadowNode>() {
        let outline = node.outline();
        let outer = graphene_rect_path(&node.bounds());
        let inner = gsk_rounded_rect_path(&outline);
        let cid = state.unique_id();
        state.add_def(html! {
            clipPath id=(cid) clipPathUnits="userSpaceOnUse" {
                path clip-rule="evenodd"
                    d={ (outer) " " (inner) }
                {}
            }
        });

        let fid = state.unique_id();
        let dev = node.blur_radius() / 2.5;
        state.add_def(html! {
            filter id=(fid) {
                feGaussianBlur in="SourceGraphic" stdDeviation=(dev) {}
            }
        });

        let spread = node.spread();
        let or = outline.bounds();
        let oc = outline.corner().to_owned();
        let or = graphene::Rect::new(
            or.x() + node.dx() - spread,
            or.y() + node.dy() - spread,
            or.width() + spread * 2.0,
            or.height() + spread * 2.0,
        );
        let d = gsk_rounded_rect_path(&gsk4::RoundedRect::new(or, oc[0], oc[1], oc[2], oc[3]));
        html! {
            g clip-path=(id_url(&cid)) {
                path filter=(id_url(&fid)) fill=(color_str(&node.color()))
                    fill-opacity=(node.color().alpha()) d=(d)
                {}
            }
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::TransformNode>() {
        let t = node.transform();
        let mat = if t.category().into_glib() >= gsk4::TransformCategory::_2d.into_glib() {
            let (xx, yx, xy, yy, dx, dy) = t.to_2d();
            format!("matrix({xx} {yx} {xy} {yy} {dx} {dy})")
        } else {
            let m = t.to_matrix();
            let m = m.values();
            let m = (0..4)
                .flat_map(|c| (0..4).map(move |r| m[r][c].to_string()))
                .collect::<Vec<_>>()
                .join(" ");
            format!("matrix3d({m})")
        };
        html! { g transform=(mat) { (transform(&node.child(), state)) } }
    } else if let Some(node) = node.downcast_ref::<gsk4::OpacityNode>() {
        html! { g opacity=(node.opacity()) { (transform(&node.child(), state)) } }
    } else if let Some(node) = node.downcast_ref::<gsk4::ColorMatrixNode>() {
        let m = node.color_matrix();
        let m = m.values();
        let m = (0..4)
            .flat_map(|r| {
                (0..4)
                    .map(move |c| m[r][c].to_string())
                    .chain(["0".into()].into_iter())
            })
            .collect::<Vec<_>>()
            .join(" ");
        let o = node.color_offset();
        let id = state.unique_id();
        state.add_def(html! {
            filter id=(id) {
                feColorMatrix type="matrix" in="SourceGraphic" values=(m) {}
                feComponentTransfer {
                    feFuncR type="linear" slope="1" intercept=(o.x()) {}
                    feFuncG type="linear" slope="1" intercept=(o.y()) {}
                    feFuncB type="linear" slope="1" intercept=(o.z()) {}
                    feFuncA type="linear" slope="1" intercept=(o.w()) {}
                }
            }
        });
        html! { g filter=(id_url(&id)) { (transform(&node.child(), state)) } }
    } else if let Some(node) = node.downcast_ref::<gsk4::RepeatNode>() {
        let b = node.bounds();
        let cb = node.child_bounds();
        let child = transform(&node.child(), state);
        let id = state.unique_id();
        state.add_def(html! {
            pattern id=(id) patternUnits="userSpaceOnUse" patternContentUnits="userSpaceOnUse"
                x=(cb.x()) y=(cb.y()) width=(cb.width()) height=(cb.height()) {}
            {
                (child)
            }
        });
        html! {
            rect fill=(id_url(&id))
                x=(b.x()) y=(b.y()) width=(b.width()) height=(b.height()) {}
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::ClipNode>() {
        let b = node.clip();
        let id = state.unique_id();
        state.add_def(html! {
            clipPath id=(id) clipPathUnits="userSpaceOnUse" {
                rect x=(b.x()) y=(b.y()) width=(b.width()) height=(b.height()) {}
            }
        });
        html! {
            g clip-path=(id_url(&id)) { (transform(&node.child(), state)) }
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::RoundedClipNode>() {
        let d = gsk_rounded_rect_path(&node.clip());
        let id = state.unique_id();
        state.add_def(html! {
            clipPath id=(id) clipPathUnits="userSpaceOnUse" {
                path d=(d) {}
            }
        });
        html! {
            g clip-path=(id_url(&id)) { (transform(&node.child(), state)) }
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::ShadowNode>() {
        let shadows = (0..node.n_shadows())
            .map(|i| node.shadow(i))
            .collect::<Vec<_>>();
        let shadow_ids = (0..shadows.len())
            .map(|i| format!("s{}", i))
            .collect::<Vec<_>>();
        let id = state.unique_id();
        state.add_def(html! {
            filter id=(id) {
                @for (i, s) in shadows.iter().enumerate() {
                    feGaussianBlur in="SourceAlpha" stdDeviation=((s.radius() / 2.5)) {}
                    feOffset dx=(s.dx()) dy=(s.dy()) result={ "b" (shadow_ids[i]) } {}
                    feFlood flood-color=(color_str(s.color())) flood-opacity=(s.color().alpha()) {}
                    feComposite in2={ "b" (shadow_ids[i]) } operator="in" result=(shadow_ids[i]) {}
                }
                feMerge {
                    @for sid in &shadow_ids {
                        feMergeNode in=(sid) {}
                    }
                    feMergeNode in="SourceGraphic" {}
                }
            }
        });
        html! { g filter=(id_url(&id)) { (transform(&node.child(), state)) } }
    } else if let Some(node) = node.downcast_ref::<gsk4::BlendNode>() {
        let mode = node.blend_mode();
        let mode = if mode == gsk4::BlendMode::Default {
            "normal".into()
        } else {
            format!("{:?}", mode).to_kebab_case()
        };
        let bid = state.unique_id();
        let bottom = transform(&node.bottom_child(), state);
        state.add_def(html! { g id=(bid) { (bottom) } });
        let id = state.unique_id();
        state.add_def(html! {
            filter id=(id) {
                feImage xlink:href={ "#" (bid) } result="img" {}
                feBlend mode=(mode) in="SourceGraphic" in2="img" {}
            }
        });
        html! {
            g filter=(id_url(&id)) { (transform(&node.top_child(), state)) }
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::CrossFadeNode>() {
        let progress = node.progress();
        if progress == 0.0 {
            transform(&node.start_child(), state)
        } else if progress == 1.0 {
            transform(&node.end_child(), state)
        } else {
            html! {
                g opacity=((1.0 - progress)) {
                    (transform(&node.start_child(), state))
                }
                g opacity=(progress) {
                    (transform(&node.end_child(), state))
                }
            }
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::TextNode>() {
        let font = node.font();
        let offset = node.offset();
        let glyphs = node.glyphs();
        let desc = font.describe().unwrap();

        let family = desc.family().unwrap_or_else(|| "".into());
        let style = format!("{:?}", desc.style()).to_kebab_case();
        let variant = desc.variant();
        let variant = if variant == pango::Variant::TitleCaps {
            "titling-caps".into()
        } else {
            format!("{:?}", variant).to_kebab_case()
        };
        let stretch = format!("{:?}", desc.stretch()).to_kebab_case();
        let mut weight = desc.weight();
        if weight == pango::Weight::Semilight || weight == pango::Weight::Book {
            weight = pango::Weight::Normal;
        } else if weight == pango::Weight::Ultraheavy {
            weight = pango::Weight::Heavy;
        };
        let weight = weight.into_glib();
        let variations = desc
            .variations()
            .unwrap_or_else(|| "".into())
            .split(',')
            .filter_map(|v| {
                if v.is_empty() {
                    return None;
                }
                if let Some((key, rest)) = v.split_once('=') {
                    Some(format!("\"{}\" {}", key, rest))
                } else {
                    Some(format!("\"{}\"", v))
                }
            })
            .collect::<Vec<_>>()
            .join(", ");
        let size = (desc.size() / pango::SCALE).to_string();
        let font_suffix = if desc.is_size_absolute() { "px" } else { "pt" };

        let index_map = state.fonts.entry(font).or_insert_with_key(|font| {
            if glib::Type::from_name("PangoFcFont")
                .map(|t| !font.type_().is_a(t))
                .unwrap_or(false)
            {
                return Default::default();
            }
            let mut index_map = HashMap::new();
            unsafe {
                let face = pango_fc_font_lock_face(font.as_ptr() as *mut _);
                if freetype_sys::FT_Select_Charmap(face, freetype_sys::FT_ENCODING_UNICODE) != 0 {
                    return Default::default();
                }
                let mut gindex = 0;
                let mut code = freetype_sys::FT_Get_First_Char(face, &mut gindex);
                while gindex != 0 {
                    if let Some(code) = char::from_u32(code as u32) {
                        index_map.insert(gindex, code);
                    }
                    code = freetype_sys::FT_Get_Next_Char(face, code, &mut gindex);
                }
                pango_fc_font_unlock_face(font.as_ptr() as *mut _);
            };
            index_map
        });
        html! {
            text x=(offset.x()) y=(offset.y()) fill=(color_str(&node.color()))
                fill-opacity=(node.color().alpha())
                font-family=(family) font-size={ (size) (font_suffix) }
                font-style=(style) font-variant=(variant) font-stretch=(stretch)
                font-weight=(weight) font-feature-settings=(variations)
            {
                @for g in glyphs {
                    @if let Some(c) = char::from_u32(g.glyph()) {
                        @if let Some(c) = index_map.get(&(c as u32)) {
                            tspan dx=((g.geometry().x_offset() / pango::SCALE))
                                dy=((g.geometry().y_offset() / pango::SCALE)) {
                                    (c)
                            }
                        }
                    }
                }
            }
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::BlurNode>() {
        let id = state.unique_id();
        state.add_def(html! {
            filter id=(id) {
                feGaussianBlur in="SourceGraphic" stdDeviation=(node.radius()) {}
            }
        });
        html! {
            g filter=(id_url(&id)) {
                (transform(&node.child(), state))
            }
        }
    } else if let Some(node) = node.downcast_ref::<gsk4::DebugNode>() {
        transform(&node.child(), state)
    } else if let Some(node) = node.downcast_ref::<gsk4::GLShaderNode>() {
        if let Some(renderer) = &state.renderer {
            let texture = renderer.render_texture(&node, None);
            let bytes = texture.save_to_png_bytes();
            let data = glib::base64_encode(&bytes);
            let b = node.bounds();
            html! {
                image xlink:href={ "data:image/png;base64," (data) }
                    x=(b.x()) y=(b.y()) width=(b.width()) height=(b.height()) {}
            }
        } else {
            let children = (0..node.n_children()).map(|c| transform(&node.child(c), state));
            html! { @for child in children { (child) } }
        }
    } else {
        eprintln!("unsupported node type {:?}", node.node_type());
        html! {}
    }
}

/// Converts a GSK node file into an SVG.
#[derive(clap::Parser)]
struct Args {
    /// Input .node file, can be saved from the "Recorder" tab in GTK inspector.
    input: PathBuf,
    /// Output SVG file. Defaults to stdout.
    #[clap(short, long)]
    output: Option<PathBuf>,
    /// Render to a Cairo SVG surface. Might produce strange results.
    #[clap(short = 'c', long)]
    force_cairo: bool,
    /// Do not render any GL shaders, just render the child nodes.
    #[clap(long)]
    ignore_gl_shaders: bool,
}

fn main() {
    gtk4::init().unwrap();
    let Args {
        input: filename,
        output,
        force_cairo,
        ignore_gl_shaders,
    } = <Args as clap::Parser>::parse();
    let (bytes, filename) = if filename.to_string_lossy() == "-" {
        let mut buf = Vec::new();
        std::io::stdin().read_to_end(&mut buf).unwrap();
        (buf, "(stdin)".into())
    } else {
        (std::fs::read(&filename).unwrap(), filename)
    };
    let bytes = glib::Bytes::from_owned(bytes);
    let mut has_err = false;
    let node = gsk4::RenderNode::deserialize_with_error_func(&bytes, |start, _end, err| {
        has_err = true;
        eprintln!(
            "{}:{}:{}: {}",
            filename.display(),
            start.lines() + 1,
            start.line_chars() + 1,
            err.message()
        );
    });
    if node.is_none() || has_err {
        eprintln!("Failed to read node from {}", filename.display());
    }
    let node = node.unwrap();
    let svg = if force_cairo {
        render_cairo_svg(&node).unwrap().into_string()
    } else {
        let mut state = TransformState::default();
        if !ignore_gl_shaders {
            state.renderer = Some(gsk4::GLRenderer::new());
        }
        let bounds = node.bounds();
        let body = transform(&node, &mut state);
        let defs = html! { @for def in state.defs { (def) }};
        format!(
            "<?xml version=\"1.0\" encoding=\"utf-8\"?>\
             <svg xmlns=\"http://www.w3.org/2000/svg\" \
                 xmlns:xlink=\"http://www.w3.org/1999/xlink\" \
                 width=\"{}\" height=\"{}\"\
            >\
                <defs>{}</defs>\
                {}\
            </svg>",
            bounds.x() + bounds.width(),
            bounds.y() + bounds.height(),
            defs.into_string(),
            body.into_string()
        )
    };
    if let Some(output) = output {
        std::fs::write(output, svg).unwrap();
    } else {
        std::io::stdout().write_all(svg.as_bytes()).unwrap();
    }
}
