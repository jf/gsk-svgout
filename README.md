# gsk-svgout

Converts directly from GSK scene graph nodes into SVG files.

GTK4 has the ability to save captures of its internal scene graph format. This
functionality can be accessed in the GTK Inspector by opening the "Recorder" tab,
recording some frames, then pressing the "Save selected node" button. This tool
converts from the saved node format into SVG files that are optimized for easy
editing in Inkscape.

### Compiling/Usage

```
cargo build --release
./target/release/gsk-svgout my_capture.node -o my_screen.svg
```

### Debugging

The `gtk4-node-editor` tool can be used to compare output from this tool and
generate PNGs rendered by GTK. See the `testsuite/gsk` folder in the GTK4
repository for the test cases.

### Sample

![Sample Image](sample.svg "Sample Image")
